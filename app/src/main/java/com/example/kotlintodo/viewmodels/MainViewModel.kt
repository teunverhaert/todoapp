package com.example.kotlintodo.viewmodels

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import com.example.kotlintodo.classes.ToDoItem
import com.icapps.architecture.arch.BaseViewModel

class MainViewModel: BaseViewModel(), LifecycleObserver {

    private val item: ToDoItem = ToDoItem("Item van view model", "description")

    fun getItem(): ToDoItem = item

}