package com.example.kotlintodo.util

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Logger @Inject constructor() {
    fun getInfo(message: String): String {
        return "Info: $message"
    }

    fun getError(message: String): String {
        return "Info: $message"
    }
}