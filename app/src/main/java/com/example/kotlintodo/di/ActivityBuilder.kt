package com.example.kotlintodo.di

import com.example.kotlintodo.activity.BaseActivity
import com.example.kotlintodo.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindBaseActivity(): BaseActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

}