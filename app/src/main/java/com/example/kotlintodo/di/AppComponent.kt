package com.example.kotlintodo.di

import com.example.kotlintodo.TodoApplication
import com.example.kotlintodo.util.Logger
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ActivityBuilder::class,
    AndroidSupportInjectionModule::class])
interface AppComponent: AndroidInjector<TodoApplication> {}