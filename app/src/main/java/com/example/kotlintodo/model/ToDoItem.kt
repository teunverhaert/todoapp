package com.example.kotlintodo.model

class ToDoItem(val title: String, val description: String?, val done: Boolean = false) {

}