package com.example.kotlintodo.activity

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.kotlintodo.R
import com.example.kotlintodo.util.Logger
import com.example.kotlintodo.databinding.ActivityMainBinding
import com.icapps.architecture.utils.ext.bindContentView
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var logger: Logger

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = R.layout.activity_main.bindContentView(this)
        binding.button.setOnClickListener {
            Toast.makeText(this, logger.getInfo("test"), Toast.LENGTH_LONG).show()
        }
    }
}
